﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiWeb.Greetings
{
    public interface ISalutationGateway
    {

        string GetMessageBasedOnAge(int age);
    }
}
