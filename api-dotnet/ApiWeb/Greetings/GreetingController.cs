﻿using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Greetings
{
    [Route("greetings")]
    public class GreetingController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            var service = new GreetingService(new SalutationGateway());
            return Ok(service.Build());
        }
    }
}