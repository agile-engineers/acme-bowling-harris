﻿
namespace ApiWeb.Greetings
{
    public class Greeting
    {
        public string Text { get; set; }
    }

    public class GreetingService
    {
        ISalutationGateway gateway;
        IEmailGateway theEmailGateway;

        public GreetingService(ISalutationGateway salutationGateway = null, IEmailGateway emailGateway = null)
        {
            gateway = salutationGateway ?? new SalutationGateway();
            theEmailGateway = emailGateway ?? new EmailGateway();
        }

        public Greeting Build(string aName = "World", int age = 0)
        {
            string hello = gateway.GetMessageBasedOnAge(age);
            theEmailGateway.SendMail("admin@mail.com", $"built a greeting for: {aName}");

            return new Greeting { Text = $"{hello}, {aName}!" };
        }
        
    }
}