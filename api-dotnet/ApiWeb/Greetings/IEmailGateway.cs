﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Greetings
{
    public interface IEmailGateway
    {
        void SendMail(string to, string body);
    }
}
