﻿namespace ApiWeb.Scores
{
    public interface IUniversalBowlingService
    {
        string GetGame(string code);
    }
}