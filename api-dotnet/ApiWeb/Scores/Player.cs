﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Scores
{
    public class Player
    {
        public Player()
        {

        }
        public Player(RawPlayer player)
        {
            Name = player.Name;
            Rolls = player.Rolls;
            Scores = BowlScorer.Score(BowlScorer.Parse(player.Rolls));
        }
        public string Name { get; set; }

        public string Rolls { get; set; }

        public int[] Scores { get; set; }

        public int Score => Scores[Scores.Length-1];
    }
}
