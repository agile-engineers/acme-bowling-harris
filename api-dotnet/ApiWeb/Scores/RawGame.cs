﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Scores
{
    public class RawGame
    {
        public string Date { get; set; }
        public RawPlayer[] Players {get; set; }

    }

    public class RawPlayer
    {
        public string Name { get; set; }

        public string Rolls { get; set; }
    }
}
