﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeb.Scores
{
    public class Game
    {
        public Game()
        {

        }

        public Game(RawGame game)
        {
            Date = game.Date;
            Players = new List<Player>();
            foreach(var player in game.Players)
            {
                Players.Add(new Player(player));
            }
        }
        public string Date { get; set; }
        public List<Player> Players {get; set; }

    }
}
