﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ApiWeb.Scores
{
    public class UniversalBowlingService : IUniversalBowlingService
    {
        public string GetGame(string code)
        {
            var url = $"https://grn4aewhhg.execute-api.us-east-1.amazonaws.com/prod/games/{code}";

            var client = new HttpClient();
            var responseText = client.GetStringAsync(url).Result;
            return responseText;
        }

        //public List<RemotePlayerData> Fetch(string code)
        //{
        //    var url = $"https://grn4aewhhg.execute-api.us-east-1.amazonaws.com/prod/games/{code}";

        //    var client = new HttpClient();
        //    var responseText = client.GetStringAsync(url).Result;

        //    var remote_players = JsonConvert.DeserializeObject<RemoteBowlingServiceResponse>(responseText);

        //    return remote_players.players;
        //}
    }
}
