﻿using ApiWeb.Scores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ApiWeb.Scores
{
    public class FakeBowlingService1 : IUniversalBowlingService
    {
        public string GetGame(string code)
        {
            if(code == "1111")
            {
                return "{ \"date\":\"2017-01-01\",\"players\":[{\"name\":\"joe\",\"rolls\":\"11111111111111111111\"},{\"name\":\"john\",\"rolls\":\"8-8-8-8-8-8-8-8-8-8-\"},{\"name\":\"jane\",\"rolls\":\"9/------------------\"}]}";
            }
            if(code == "DECK")
            {
                return "{ \"date\":\"2018-07-27\",\"players\":[{\"name\":\"Decker\",\"rolls\":\"XXXXXXXXXXXX\"}]}";
            }
            
            return "{ \"errorMessage\":\"404\"}";
        }
    }
}
