﻿using System;
using System.Runtime.Serialization;

namespace ApiWeb.Scores
{
    [Serializable]
    public class BadBowlingException : Exception
    {
        public BadBowlingException()
        {
        }

        public BadBowlingException(string message) : base(message)
        {
        }

        public BadBowlingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BadBowlingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}