﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ApiWeb.Scores
{
    public class BowlService
    {
        IUniversalBowlingService theUniversalService;

        Dictionary<string, Game> saved;

        public BowlService (IUniversalBowlingService service)
        {
            theUniversalService = service;
            saved = new Dictionary<string, Game>();
        }

        public Game GetGame(string code)
        {
            if(saved.ContainsKey(code))
            {
                return saved[code];
            }
            //^[a-zA-Z0-9]{4}$
            var regex = new Regex(@"^[a-zA-Z0-9]{4}$");
            if (regex.Matches(code).Count == 0)
            {
                throw new BadBowlingException("Invalid Code");
            }

            string raw = theUniversalService.GetGame(code);
            if(raw.Contains("error"))
            {
                return new Game();
            }

            RawGame game = JObject.Parse(raw).ToObject<RawGame>();
            var goodGame = new Game(game);

            saved.Add(code, goodGame);

            return goodGame;
        }
    }
}
