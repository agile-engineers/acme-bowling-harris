﻿using System;
using System.Collections.Generic;

namespace ApiWeb.Scores
{
    public static class BowlScorer
    {
        public static int[] Parse(string score)
        {
            char[] values = score.ToCharArray();
            int[] scores = new int[values.Length];

            int i = 0;
            foreach(char val in values)
            {
                if (val == '-')
                {
                    scores[i++] = 0;
                }
                else if (val == 'X')
                {
                    scores[i++] = 10;
                }
                else if (val == '/')
                {
                    scores[i] = 10 - scores[i - 1];
                    i++;
                }
                else
                {
                    scores[i++] = Convert.ToInt32(char.GetNumericValue(val));
                }
            }

            return scores;
        }

        // frame scores, accumulating, last number is sum
        public static int[] Score(int[] balls)
        {

            List<int> frames = new List<int>();
            var lastScore = 0;
            for (int i = 0; i < balls.Length - 1; i += 2)
            {
                var first = balls[i];
                var second = balls[i + 1];
                var score = 0;

                if(frames.Count == 10)
                {
                    // Handle 10th frame
                    second = balls.Length > i + 1 ? balls[i + 1] : 0;
                    var third = balls.Length > i + 2 ? balls[i + 2] : 0;
                    score = first + second + third;
                    break;
                }
                // Strike?
                if(first == 10)
                {
                    if( i < balls.Length - 2)
                    {
                        score = first + balls[i + 1] + balls[i + 2] + lastScore;
                    }
                    i = i - 1;
                }
                // Is a spare
                else if (first + second == 10)
                {
                    // Is there another frame?
                    if (i < balls.Length - 2)
                    {
                        score = first + second + balls[i + 2] + lastScore;
                    }
                }
                // Open Frame
                else
                {
                    score = first + second + lastScore;
                }
                frames.Add(score);
                lastScore = score;
            }

            return frames.ToArray();
        }
        
    }
}