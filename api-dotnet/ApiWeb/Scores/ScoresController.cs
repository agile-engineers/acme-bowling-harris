﻿using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Scores
{
    [Route("scores")]
    public class ScoresController : Controller
    {
        IUniversalBowlingService theUniversalService;
        public ScoresController(IUniversalBowlingService service)
        {
            theUniversalService = service;
        }

        [HttpGet("{code}")]
        public IActionResult Get(string code)
        {
            var bowlService = new BowlService(theUniversalService);
            return Ok(bowlService.GetGame(code));
        }
    }
}