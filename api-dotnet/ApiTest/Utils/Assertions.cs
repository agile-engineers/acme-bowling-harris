using Newtonsoft.Json;
using Xunit;

namespace ApiTest.Utils
{
    public class Assertions
    {
        public static void DeepEquals(object expected, object actual)
        {
            Assert.Equal(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual));
        }
    }
}