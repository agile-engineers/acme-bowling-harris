﻿using ApiTest.Utils;
using ApiWeb.Scores;
using System.Collections.Generic;
using Xunit;

namespace ApiTest.Scores
{
    public class BowlServiceTests
    {
        // given valid code, give me data
        //  date, list of players, name, rolls
        [Fact]
        public void given_a_valid_code_return_game()
        {
            var code = "DECK";
            var bowlService = new BowlService(new FakeBowlingService());

            var game = bowlService.GetGame(code);
            var Decker = new Player()
            {
                Name = "Decker",
                Rolls = "XXXXXXXXXXXX",
                Scores = BowlScorer.Score(BowlScorer.Parse("XXXXXXXXXXXX"))
        };

            var players = new List<Player>();
            players.Add(Decker);

            var expected = new Game()
            {
                Date = "2018-07-27",
                Players = players
            };

            Assertions.DeepEquals(expected, game);
        }

        [Fact]
        public void given_a_valid_code_return_game_for_multiple()
        {
            var code = "1111";
            var bowlService = new BowlService(new FakeBowlingService());

            var game = bowlService.GetGame(code);
            var player1 = new Player()
            {
                Name = "joe",
                Rolls = "11111111111111111111",
                Scores = BowlScorer.Score(BowlScorer.Parse("11111111111111111111"))
            };
            var player2 = new Player()
            {
                Name = "john",
                Rolls = "8-8-8-8-8-8-8-8-8-8-",
                Scores = BowlScorer.Score(BowlScorer.Parse("8-8-8-8-8-8-8-8-8-8-"))
            };
            var player3 = new Player()
            {
                Name = "jane",
                Rolls = "9/------------------",
                Scores = BowlScorer.Score(BowlScorer.Parse("9/------------------"))
            };

            var players = new List<Player>();
            players.Add(player1);
            players.Add(player2);
            players.Add(player3);

            var expected = new Game()
            {
                Date = "2017-01-01",
                Players = players
            };

            Assertions.DeepEquals(expected, game);
        }

        // Bad code, throw exception //custom exception
        [Fact]
        public void given_an_invalid_code_throw_excpetion()
        {
            var code = "!@#$";
            var bowlService = new BowlService(new FakeBowlingService());

            Assert.Throws<BadBowlingException>(() => bowlService.GetGame(code));

        }

        // unknown code, recieve exception, handle it
        [Fact]
        public void given_an_unknown_code_get_error()
        {
            var code = "1234";
            var bowlService = new BowlService(new FakeBowlingService());

            var actual = bowlService.GetGame(code);

            Assertions.DeepEquals(new Game(), actual);

        }

        // Re-used valid code ???
    }
}
