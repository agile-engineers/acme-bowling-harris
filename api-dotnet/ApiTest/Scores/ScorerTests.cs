﻿using ApiTest.Utils;
using ApiWeb.Scores;
using Xunit;

namespace ApiTest.Scores
{
    public class ScorerTests
    {
        // Sum numbers
        [Fact]
        public void sum_frames()
        {
            Assertions.DeepEquals(new int[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, BowlScorer.Score(new int[] { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Assertions.DeepEquals(new int[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 }, BowlScorer.Score(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }));
            Assertions.DeepEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, BowlScorer.Score(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }

        // spares
        [Fact]
        public void sum_spares()
        {
            Assertions.DeepEquals(new int[] { 11, 12, 12, 12, 12, 12, 12, 12, 12, 12 }, BowlScorer.Score(new int[] { 9, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Assertions.DeepEquals(new int[] { 19, 29, 29, 29, 29, 29, 29, 29, 29, 29 }, BowlScorer.Score(new int[] { 9, 1, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Assertions.DeepEquals(new int[] { 0, 10, 10, 10, 10, 10, 10, 10, 10, 10 }, BowlScorer.Score(new int[] { 0, 0, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }

        // strikes
        [Fact]
        public void sum_strikes()
        {
            Assertions.DeepEquals(new int[] { 12, 14, 14, 14, 14, 14, 14, 14, 14, 14 }, BowlScorer.Score(new int[] { 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Assertions.DeepEquals(new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }, BowlScorer.Score(new int[] { 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
            Assertions.DeepEquals(new int[] { 30, 51, 63, 65, 65, 65, 65, 65, 65, 65 }, BowlScorer.Score(new int[] { 10, 10, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        }

        // final scoring check
        [Fact]
        public void sum_final_frames()
        {
            Assertions.DeepEquals(new int[] { 0,0,0,0,0,0,0,0,0,30 }, BowlScorer.Score(new int[] { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 10, 10, 10 }));
            Assertions.DeepEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 7 }, BowlScorer.Score(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 4 }));
            Assertions.DeepEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 14 }, BowlScorer.Score(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 7, 4 }));
            Assertions.DeepEquals(new int[] { 30,60,90,120,150,180,210,240,270,300 }, BowlScorer.Score(new int[] { 10,10,10,10,10,10,10,10, 10, 10, 10, 10 }));
        }

        [Fact]
        public void no_scores()
        {
            Assert.NotNull(BowlScorer.Score(new int[0]));
        }
    }
}
