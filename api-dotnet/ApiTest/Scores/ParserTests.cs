﻿using ApiTest.Utils;
using ApiWeb.Scores;
using Xunit;

namespace ApiTest.Scores
{
    public class ParserTests
    {
        // Convert pins to numbers
        [Fact]
        public void convert_pin_to_number()
        {
            Assert.Equal(new int[] { 1 }, BowlScorer.Parse("1"));
            Assert.Equal(new int[] { 5 }, BowlScorer.Parse("5"));
        }

        // Convert - to 0
        [Fact]
        public void conert_dash_to_zero()
        {
            Assert.Equal(new int[] { 0 }, BowlScorer.Parse("-"));
        }

        // Convert large strings
        [Fact]
        public void convert_multilpe_pins_to_numbers()
        {
            Assertions.DeepEquals(new int[] { 1, 1 }, BowlScorer.Parse("11"));
            Assertions.DeepEquals(new int[] { 4, 2 }, BowlScorer.Parse("42"));
            Assertions.DeepEquals(new int[] { 8, 3, 5, 6, 3, 2 }, BowlScorer.Parse("835632"));
        }

        // Convert X to 10
        [Fact]
        public void convert_x_to_ten()
        {
            Assertions.DeepEquals(new int[] { 10 }, BowlScorer.Parse("X"));
        }

        // Convert / to 10-previous
        [Fact]
        public void convert_spares_to_numbers()
        {
            Assertions.DeepEquals(new int[] { 4, 6 }, BowlScorer.Parse("4/"));
        }

        // Convert complex strings
        [Fact]
        public void convert_multilpe_types_to_numbers()
        {
            Assertions.DeepEquals(new int[] { 1, 0 }, BowlScorer.Parse("1-"));
            Assertions.DeepEquals(new int[] { 4, 10 }, BowlScorer.Parse("4X"));
            Assertions.DeepEquals(new int[] { 8, 10, 5, 0, 0, 2 }, BowlScorer.Parse("8X5--2"));
            Assertions.DeepEquals(new int[] { 8, 10, 5, 8, 2, 2 }, BowlScorer.Parse("8X58/2"));
            Assertions.DeepEquals(new int[] { 10, 10, 10, 4, 3, 8, 2, 8, 2, 10, 10, 10, 5, 5, 9 }, BowlScorer.Parse("XXX438/8/XXX5/9"));
        }
    }
}