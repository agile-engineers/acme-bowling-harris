using ApiWeb.Greetings;
using Moq;
using Xunit;

namespace ApiTest.Greetings
{
    public class GreetingServiceTests
    {
        [Fact]
        public void build_a_default_greeting()
        {
            var service = new GreetingService();

            Greeting actual = service.Build();

            // Then
            Assert.Equal("Hello, World!", actual.Text);
        }

        // Hello, {Name}!!

        [Fact]
        public void build_a_personalized_greeting()
        {
            // Given
            string name = "Decker";

            var service = new GreetingService();

            // When
            Greeting actual = service.Build(name);

            // Then
            Assert.Equal("Hello, Decker!", actual.Text);
        }

        [Fact]
        public void build_a_greeting_based_on_name_and_age()
        {
            var salutationGateway = new Mock<ISalutationGateway>();

            salutationGateway
                .Setup(ds => ds.GetMessageBasedOnAge(45))
                .Returns("Whazzup");

            var sut = new GreetingService(salutationGateway.Object);

            Greeting actual = sut.Build("Joe", 45);

            Assert.Equal("Whazzup, Joe!", actual.Text);
        }

        [Fact]
        public void send_a_notification_email_when_building_greeting()
        {
            var salutationGateway = new Mock<ISalutationGateway>();
            var emailGateway = new Mock<IEmailGateway>();

            var sut = new GreetingService(
                salutationGateway.Object, emailGateway.Object);

            sut.Build("Joe");

            emailGateway.Verify(es =>
                es.SendMail("admin@mail.com", "built a greeting for: Joe"));
        }
    }
}