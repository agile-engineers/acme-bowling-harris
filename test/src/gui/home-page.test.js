const { test, suite, beforeEach, afterEach } = require( "mocha" )
const { assertTextAs, initializeWebDriver } = require( "./utils" )

suite( "home page", function () {

    beforeEach( function () {
        this.driver = initializeWebDriver()

        return this.driver
            .url( "http://localhost:8080/" )
    } )

    afterEach( function () {
        return this.driver.end()
    } )

    test( "a default greeting is displayed", function () {
        return this.driver.then( assertTextAs( "p", "Hello, World!" ) )
    } )

} )