export const start = function () {
    console.log( "index::start" )

    fetch( "http://localhost:8081/greetings" )
        .then( ( response ) => response.json() )
        .then( function ( greeting ) {
            $( "#greeting" ).html( greeting.text )
        } )
}